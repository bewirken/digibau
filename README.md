# Digibau-Wordpress

Dieses Repository beinhaltet das ```digibau-plugin``` und das ```digibau-theme``` als Git-Submodule. Eine WordPress-Entwicklungs- und Testumgebung lässt sich über ```wp-env``` starten.

## Entwicklung

### Repositorys klonen

Das Hauptrepository kann mitsamt der beiden Submodule mit 

    git clone --recurse-submodules git@gitlab.com:bewirken/digibau.git

geklont werden.

### Testsystem starten

In diesem Verzeichnis auf der Kommandozeile

    wp-env start

ausführen.
